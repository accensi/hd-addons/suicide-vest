### Notes
---
- Vests can only be found in backpacks.
- Loadout code is `svs`.
- If active, it will detonate 3 seconds after you die. Virtually useless in singleplayer.